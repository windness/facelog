// ______________________________________________________
// Generated by sql2java - https://github.com/10km/sql2java-2-6-7 (custom branch) 
// modified by guyadong from
// sql2java original version https://sourceforge.net/projects/sql2java/ 
// JDBC driver used at code generation time: com.mysql.jdbc.Driver
// template: sub.adapters.java.vm
// ______________________________________________________
package net.gdface.facelog.client;

import gu.simplemq.IMessageAdapter;
import gu.simplemq.exceptions.SmqUnsubscribeException;
import gu.simplemq.redis.RedisSubscriber;
import gu.simplemq.Channel;

import net.gdface.facelog.db.PermitBean;

/**
 * 数据库表记录变化订阅对象父类集合,应用项目根据需要继承相应的类实现自己的业务逻辑
 * @author guyadong
 *
 */
public class SubAdapters implements ChannelConstant{    
    /**
     * 订阅 fl_person 表的 Insert 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BasePersonInsertSubAdapter implements IMessageAdapter<Integer>{
        /** 订阅频道 */
        protected final Channel<Integer> channel = PUBSUB_PERSON_INSERT.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BasePersonInsertSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BasePersonInsertSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param id 用户id
         */
        @Override
        public void onSubscribe(Integer id) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_person 表的 Update 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BasePersonUpdateSubAdapter implements IMessageAdapter<Integer>{
        /** 订阅频道 */
        protected final Channel<Integer> channel = PUBSUB_PERSON_UPDATE.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BasePersonUpdateSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BasePersonUpdateSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param id 用户id
         */
        @Override
        public void onSubscribe(Integer id) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_person 表的 Delete 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BasePersonDeleteSubAdapter implements IMessageAdapter<Integer>{
        /** 订阅频道 */
        protected final Channel<Integer> channel = PUBSUB_PERSON_DELETE.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BasePersonDeleteSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BasePersonDeleteSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param id 用户id
         */
        @Override
        public void onSubscribe(Integer id) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_feature 表的 Insert 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BaseFeatureInsertSubAdapter implements IMessageAdapter<String>{
        /** 订阅频道 */
        protected final Channel<String> channel = PUBSUB_FEATURE_INSERT.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BaseFeatureInsertSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BaseFeatureInsertSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param md5 主键,特征码md5校验码
         */
        @Override
        public void onSubscribe(String md5) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_feature 表的 Update 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BaseFeatureUpdateSubAdapter implements IMessageAdapter<String>{
        /** 订阅频道 */
        protected final Channel<String> channel = PUBSUB_FEATURE_UPDATE.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BaseFeatureUpdateSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BaseFeatureUpdateSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param md5 主键,特征码md5校验码
         */
        @Override
        public void onSubscribe(String md5) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_feature 表的 Delete 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BaseFeatureDeleteSubAdapter implements IMessageAdapter<String>{
        /** 订阅频道 */
        protected final Channel<String> channel = PUBSUB_FEATURE_DELETE.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BaseFeatureDeleteSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BaseFeatureDeleteSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param md5 主键,特征码md5校验码
         */
        @Override
        public void onSubscribe(String md5) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_permit 表的 Insert 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BasePermitInsertSubAdapter implements IMessageAdapter<PermitBean>{
        /** 订阅频道 */
        protected final Channel<PermitBean> channel = PUBSUB_PERMIT_INSERT.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BasePermitInsertSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BasePermitInsertSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param permit fl_permit 表记录
         */
        @Override
        public void onSubscribe(PermitBean permit) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_permit 表的 Update 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BasePermitUpdateSubAdapter implements IMessageAdapter<PermitBean>{
        /** 订阅频道 */
        protected final Channel<PermitBean> channel = PUBSUB_PERMIT_UPDATE.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BasePermitUpdateSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BasePermitUpdateSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param permit fl_permit 表记录
         */
        @Override
        public void onSubscribe(PermitBean permit) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    /**
     * 订阅 fl_permit 表的 Delete 消息,当表数据变化时执行相应的业务逻辑
     * @author guyadong
     *
     */
    public static class BasePermitDeleteSubAdapter implements IMessageAdapter<PermitBean>{
        /** 订阅频道 */
        protected final Channel<PermitBean> channel = PUBSUB_PERMIT_DELETE.asMutable().setAdapter(this);
        /**
         * 向{@code subscriber}注册,注册后才能收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#register(Channel...)
         */
        public BasePermitDeleteSubAdapter register(RedisSubscriber subscriber){
            subscriber.register(channel);
            return this;
        }
        /**
         * 向{@code subscriber}注销订阅的频道,注销后不会再收到订阅消息
         * @param subscriber 
         * @return
         * @see RedisSubscriber#unregister(Channel...)
         */
        public BasePermitDeleteSubAdapter unregister(RedisSubscriber subscriber){
            subscriber.unregister(channel);
            return this;
        }
        /** 
         * 应用项目重写此方法实现业务逻辑 
         * @param permit fl_permit 表记录
         */
        @Override
        public void onSubscribe(PermitBean permit) throws SmqUnsubscribeException {
            // 实现业务逻辑
        }
    }    
    // no code
}
